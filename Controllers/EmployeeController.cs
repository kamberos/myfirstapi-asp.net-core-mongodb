﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyFirstAPI.Models;
using MyFirstAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace MyFirstAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService _employeeService;

        public EmployeeController(EmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        // GET
        [HttpGet]
        public ActionResult<List<employee>> Get() =>
            _employeeService.Get();

        [HttpGet("{id:length(24)}")]
        public ActionResult<employee> Get(string id)
        {
            var empl = _employeeService.Get(id);
            if (empl == null)
            {
                return NotFound();
            }
            return empl;
        }

        // POST
        [HttpPost]
        public ActionResult<employee> Create(employee empl)
        {
            //using (var item = new employee()) {
           // }
            _employeeService.Create(empl);
            return CreatedAtRoute("GetBook", new { id = empl.Id.ToString()}, empl);
        }
        // Remove
        //[HttpDelete("id:length(24)")]
        //public IActionResult Delete(string id)
        //{
        //    var empl = _employeeService.Get(id);
        //    return null;
        //}
    }
}
