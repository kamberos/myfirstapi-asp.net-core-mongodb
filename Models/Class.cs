﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace MyFirstAPI.Models
{
    public class employee
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("name")]
        public string EmployeeName { get; set; }
        public string surname { get; set; }
        public string Password { get; set; }
        public string position { get; set; }
    }
}
