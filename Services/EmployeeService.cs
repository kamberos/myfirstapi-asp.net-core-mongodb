﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyFirstAPI.Models;
using MongoDB.Driver;

namespace MyFirstAPI.Services
{
    public class EmployeeService
    {
        private readonly IMongoCollection<employee> _employees;

        public EmployeeService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _employees = database.GetCollection<employee>(settings.BooksCollectionName);
        }

        public List<employee> Get() =>
            _employees.Find(emplo => true).ToList();

        public employee Get(string id) =>
            _employees.Find<employee>(empl => empl.Id == id).FirstOrDefault();

        public employee Create(employee empl)
        {
            _employees.InsertOne(empl);
            return empl;
        }
        public void Remove(string id) =>
            _employees.DeleteOne(empl => empl.Id == id);

    }
    }
